<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Controls extends CI_Controller {
	
	function __construct()	{
		parent::__construct();
		$this->load->model('models');
		require_once APPPATH."third_party/dompdf/dompdf_config.inc.php";

	}

	public function index(){
		$this->load->view('login');
		
	}
	
	public function dologin(){
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username','username','trim|required');
			$this->form_validation->set_rules('password','password','trim|required');

			if ($this->form_validation->run()) {
				if ($this->models->login() ==  TRUE) {

					$data_session = array(
						'username' => $username,
						'status' => 'login'
					);

					$this->session->set_userdata($data_session);
					redirect(base_url('dashboard'));
				}else{
					$data['notif'] = 'Username Atau Password Salah';
					$this->load->view('login',$data);
				}
			}else {
				$data['notif'] = validation_errors();
				$this->load->view('login',$data);
			}
		}
	}

	public function dashboard(){
		$this->load->view('header');
		$data = $this->models->pel();
		$data1 = $this->models->tag();
		$data2 = $this->models->bay();
		$data3 = $this->models->tung();
		$data = array(
			'pelanggan' => $data[0]['pelanggan'],
			'tagihan' => $data1[0]['tagihan'],
			'bayar' => $data2[0]['bayar'],
			'tunggakan' => $data3[0]['tunggakan']
		);
		$this->load->view('dashboard',$data);
		$this->load->view('footer');
		
	}

	public function usr(){
		$this->load->view('header');
		$data = $this->models->data('user');
		$data = array('data' => $data);
		$this->load->view('username', $data);
		$this->load->view('footer');
	}

	public function addusr(){
		$this->load->view('header');
		$this->load->view('addusr');
		$this->load->view('footer');
	}

	public function updtusr($id_us){
		$this->load->view('header');
		$obat = $this->models->getwhere('user', array('id_us' => $id_us ));
		$data  = array('id_us' => $obat[0]['id_us'],
			'nama_us' => $obat[0]['nama_us']
		);
		$this->load->view('updateusr', $data);
		$this->load->view('footer');

	}

	public function dodelusr($data){
		$data = array('id_us' => $data);
		$this->models->delete('user',$data);
		redirect(base_url('usr'));
	}

	public function insertusr(){
		$nama_us = $_POST['nama_us'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$data = array('nama_us' => $nama_us,
			'username' => $username,
			'password' => md5($password)
		);
		$data = $this->models->add('user',$data);
		redirect(base_url('usr'));
	}

	public function updateuser(){
		$id_us = $_POST['id_us'];
		$nama_us = $_POST['nama_us'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$data = array(
			'nama_us' => $nama_us,
			'username' => $username,
			'password' => md5($password)
		);

		$where = array(
			'id_us' => $id_us,
		);

		$res = $this->models->update('user',$data, $where);
		if ($res > 0) {

			redirect(base_url("usr"));
		}
	}

	public function doadd(){
		$this->load->view('header');
		$this->load->view('addpel');
		$this->load->view('footer');
	}

	public function dodata(){
		$this->load->view('header');
		$data = $this->models->data('pelanggan');
		$data = array('data' => $data);
		$this->load->view('pel', $data);
		$this->load->view('footer');

	}

	public function insert(){
		$data = array('nama_pel' => $this->input->post('nama_pel'),
			'alamat_pel' => $this->input->post('alamat_pel')
		);
		$data = $this->models->add('pelanggan',$data);
		redirect(base_url('dodata'));		
	}

	
	public function updtpel($id_pel){
		$this->load->view('header');
		$obat = $this->models->getwhere('pelanggan', array('id_pel' => $id_pel ));
		$data  = array('id_pel' => $obat[0]['id_pel'],
			'nama_pel' => $obat[0]['nama_pel'],
			'alamat_pel' => $obat[0]['alamat_pel'],
		);
		$this->load->view('updatepel', $data);
		$this->load->view('footer');

	}

	public function update(){
		$id_pel = $_POST['id_pel'];
		$nama_pel = $_POST['nama_pel'];
		$alamat_pel = $_POST['alamat_pel'];
		$data = array(
			'nama_pel' => $nama_pel,
			'alamat_pel' => $alamat_pel
		);

		$where = array(
			'id_pel' => $id_pel,
		);

		$res = $this->models->update('pelanggan',$data, $where);
		if ($res > 0) {

			redirect(base_url("index.php/controls/dodata"));
		}
	}

	public function dodelpel($data){
		$data = array('id_pel' => $data);
		$this->models->delete('pelanggan',$data);
		redirect(base_url('dodata'));
	}

	public function addgol(){
		$this->load->view('header');
		$this->load->view('addgol');
		$this->load->view('footer');
	}

	public function dogol(){
		$this->load->view('header');
		$data = $this->models->data('harga')	;
		$data = array('data' => $data);
		$this->load->view('gol', $data);
		$this->load->view('footer');

	}

	public function insertgol(){
		$data = array('nama_harga' => $this->input->post('nama_harga'),
			'harga' => $this->input->post('harga')
		);
		$data = $this->models->add('harga',$data);
		redirect(base_url('dogol'));
	}

	

	public function updtgol($id_harga){
		$this->load->view('header');
		$obat = $this->models->getwhere('harga', array('id_harga' => $id_harga ));
		$data  = array('id_harga' => $obat[0]['id_harga'],
			'nama_harga' => $obat[0]['nama_harga'],
			'harga' => $obat[0]['harga'],
		);
		$this->load->view('updategol', $data);
		$this->load->view('footer');

	}

	public function updategol(){
		$id_harga = $_POST['id_harga'];
		$nama_harga = $_POST['nama_harga'];
		$harga = $_POST['harga'];
		$data = array(
			'nama_harga' => $nama_harga,
			'harga' => $harga
		);

		$where = array(
			'id_harga' => $id_harga,
		);

		$res = $this->models->update('harga',$data, $where);
		if ($res > 0) {

			redirect(base_url("dogol"));
		}
	}

	public function dodelgol($data){
		$data = array('id_harga' => $data);
		$this->models->delete('harga',$data);
		redirect(base_url('index.php/controls/dogol'));
	}

	public function dobbn(){
		$this->load->view('header');
		$data = $this->models->data('beban');
		$data = array('data' => $data);
		$this->load->view('bbn', $data);
		$this->load->view('footer');

	}

	public function addbbn(){
		$this->load->view('header');
		$this->load->view('addbbn');
		$this->load->view('footer');
	}

	public function insertbbn(){
		$data = array('harga_bbn' => $this->input->post('harga_bbn')
	);
		$data = $this->models->add('beban',$data);
		redirect(base_url('dobbn'));
	}

	
	public function updtbbn($id_bbn){
		$this->load->view('header');
		$obat = $this->models->getwhere('beban', array('id_bbn' => $id_bbn ));
		$data  = array('id_bbn' => $obat[0]['id_bbn'],
			'harga_bbn' => $obat[0]['harga_bbn'],
		);
		$this->load->view('updatebbn', $data);
		$this->load->view('footer');

	}

	public function updatebbn(){
		$id_bbn = $_POST['id_bbn'];
		$harga_bbn = $_POST['harga_bbn'];
		$data = array(
			'harga_bbn' => $harga_bbn
		);

		$where = array(
			'id_bbn' => $id_bbn,
		);

		$res = $this->models->update('beban',$data, $where);
		if ($res > 0) {

			redirect(base_url("dobbn"));
		}
	}

	public function dodelbbn($data){
		$data = array('id_bbn' => $data);
		$this->models->delete('beban',$data);
		redirect(base_url('dobbn'));
	}
	public function dodelkttp($data){
		$data = array('id_trans' => $data);
		$this->models->delete('transaksi',$data);
		redirect(base_url('kettp'));
	}

	public function pen(){
		$this->load->view('header');
		$data = $this->models->data('pelanggan');
		$data = array('data' => $data);
		$this->load->view('penetapan', $data);
		$this->load->view('footer');
	}

	public function penadd($id_pel){
		$this->load->view('header');
		$obat = $this->models->getwhere('pelanggan', array('id_pel' => $id_pel ));
		$hrg = $this->models->data('harga');
		$bbn = $this->models->data('beban');
		$tgg = $this->models->getwhere('tunggakan', array('id_pel' => $id_pel ));
		$data  = array('id_pel' => $obat[0]['id_pel'],
			'nama_pel' => $obat[0]['nama_pel'],
			'alamat_pel' => $obat[0]['alamat_pel'],
			'data' => $hrg,
			'harga_bbn' => $bbn[0]['harga_bbn'],
			'id_bbn' => $bbn[0]['id_bbn'],
			'data1' => $tgg
		);
		$this->load->view('penadd', $data);
		// print_r($data);
		$this->load->view('footer');

	}

	public function trans(){

		$id_harga = $_POST['id_harga'];
		$id_pel = $_POST['id_pel'];
		$id_bbn = $_POST['id_bbn'];
		$masa_awal = $_POST['masa_awal'];
		$masa_akhir = $_POST['masa_akhir'];
		$volume = $_POST['volume'];
		$tagihan = $_POST['jumlah'];
		$total = $_POST['total'];
		$jmlh_tunggakan = $_POST['tung'];

		$data = array('id_pel' => $id_pel,
			'id_harga' => $id_harga,
			'id_bbn' => $id_bbn,
			'masa_awal' => $masa_awal,
			'masa_akhir' => $masa_akhir,
			'volume' => $volume,
			'tagihan' => $tagihan,
			'total' => $total

		);

		$data = $this->models->add('transaksi',$data);

		$id_tung = $_POST['id_tung'];
		if ($id_tung == NULL) {

			$data1 = array(
				'id_pel' => $id_pel,
				'jmlh_tunggakan' => $jmlh_tunggakan
			);

			$data1 = $this->models->add('tunggakan', $data1);

		} else {

			$data = array(
				'jmlh_tunggakan' => $jmlh_tunggakan
			);

			$where = array(
				'id_tung' => $id_tung,
			);

			$res = $this->models->update('tunggakan',$data, $where);

		}
		
		redirect(base_url('pen'));
	}

	public function bayar($id_trans){
		$this->load->view('header');
		$obat = $this->models->getwhere('transaksi', array('id_trans' => $id_trans ));
		$data = $this->models->pem($id_trans);
		$data  = array('id_trans' => $obat[0]['id_trans'],
			'id_pel' => $obat[0]['id_pel'],
			'nama_pel' => $data[0]['nama_pel'],
			'alamat_pel' => $data[0]['alamat_pel'],
			'masa_awal' => $obat[0]['masa_awal'],
			'masa_akhir' => $obat[0]['masa_akhir'],
			'tagihan' => $obat[0]['tagihan'],
			'tunggakan' =>$data[0]['jmlh_tunggakan'],
			'volume' => $obat[0]['volume'],
			'total' => $obat[0]['total'],
			'data1' => $data
		);
		$this->load->view('bayar', $data);
		$this->load->view('footer');
	}

	public function updatebayar(){

		$id_trans = $_POST['id_trans'];
		$id_pel = $_POST['id_pel'];
		$nama_pel = $_POST['nama_pel'];
		$alamat_pel = $_POST['alamat_pel'];
		$masa = $_POST['masa'];
		$volume = $_POST['volume'];
		$tagihan = $_POST['jumlah'];
		$tunggakan = $_POST['tunggakan'];
		$bayar = $_POST['bayar'];
		$jmlh_tunggakan = $_POST['tagihan'];
		$id_tung = $_POST['id_tung'];
		$tgl_bayar = date('y-m-d');

		$data = array(
			'bayar' => $bayar,
			'tgl_bayar' => $tgl_bayar
		);

		$where = array(
			'id_trans' => $id_trans,
		);

		$res = $this->models->update('transaksi',$data, $where);
		$id_tung = $_POST['id_tung'];
		if ($id_tung == NULL) {

			$data1 = array(
				'id_pel' => $id_pel,
				'jmlh_tunggakan' => $jmlh_tunggakan
			);

			$data1 = $this->models->add('tunggakan', $data1);

		} else {

			$data = array(
				'jmlh_tunggakan' => $jmlh_tunggakan
			);

			$where = array(
				'id_tung' => $id_tung,
			);

			$res = $this->models->update('tunggakan',$data, $where);

		}

		$data  = array(
			'id_trans' => $id_trans,
			'nama_pel' => $nama_pel,
			'alamat_pel' => $alamat_pel,
			'volume' => $volume,
			'masa' => $masa,
			'tagihan' => $tagihan,
			'bayar' => $bayar,
			'total' => $total,
			'tgl_bayar' => $tgl_bayar,
			'jmlh_tunggakan' => $jmlh_tunggakan,
			'id_tung' => $id_tung

		);
		$res = $this->load->view('printbay', $data);

		redirect(base_url("kettp"));
	}

	public function pemb(){
		$this->load->view('header');
		$data = $this->models->datpem();
		$data = array('data' => $data);
		$this->load->view('pembayaran', $data);
		$this->load->view('footer');
	}

	public function kettp(){
		$this->load->view('header');
		$data = $this->models->datpem();
		$data = array('data' => $data);
		$this->load->view('ketetapan', $data);
		$this->load->view('footer');
	}

	public function updte($id_trans){
		$this->load->view('header');
		$obat = $this->models->getwhere('transaksi', array('id_trans' => $id_trans ));
		$data = $this->models->pem($id_trans);
		$hrg = $this->models->data('harga');
		$bbn = $this->models->data('beban');
		$data  = array('id_trans' => $obat[0]['id_trans'],
			'id_pel' => $obat[0]['id_pel'],
			'nama_pel' => $data[0]['nama_pel'],
			'alamat_pel' => $data[0]['alamat_pel'],
			'masa_awal' => $obat[0]['masa_awal'],
			'masa_akhir' => $obat[0]['masa_akhir'],
			'volume' => $obat[0]['volume'],
			'tagihan' => $obat[0]['tagihan'],
			'id_tung' => $data[0]['id_tung'],
			'jmlh_tunggakan'=> $data[0]['jmlh_tunggakan'],
			'data' => $hrg,
			'id_bbn' => $bbn[0]['id_bbn'],
			'harga_bbn' => $bbn[0]['harga_bbn'],
			'total' => $obat[0]['total']

		);
		$this->load->view('updatektp', $data);
		$this->load->view('footer');
	}

	public function hpsktp($id_trans){
		$this->load->view('header');
		$obat = $this->models->getwhere('transaksi', array('id_trans' => $id_trans ));
		$data = $this->models->pem($id_trans);
		$data  = array('id_trans' => $obat[0]['id_trans'],
			'id_pel' => $obat[0]['id_pel'],
			'nama_pel' => $data[0]['nama_pel'],
			'alamat_pel' => $data[0]['alamat_pel'],
			'tagihan' => $data[0]['tagihan'],
			'bayar' => $data[0]['bayar'],
			'jmlh_tunggakan' => $data[0]['jmlh_tunggakan'],
			'id_tung' => $data[0]['id_tung']

		);
		$this->load->view('hapus', $data);
		$this->load->view('footer');
	}

	public function hapuskettp(){
		$id_trans = $_POST['id_trans'];
		$id_tung = $_POST['id_tung'];
		$jmlh_tunggakan = $_POST['tagihan'];
		$data = array(
			'jmlh_tunggakan' => $jmlh_tunggakan
		);

		$where = array(
			'id_tung' => $id_tung,
		);

		$res = $this->models->update('tunggakan',$data, $where);

		$data1 = array(
			'id_trans' => $id_trans
		);
		$res1 = $this->models->delete('transaksi',$data1);

		if ($res1 > 0) {
			redirect(base_url("index.php/controls/kettp"));
		}
	}

	public function updateketetapan() {
		$id_trans = $_POST['id_trans'];
		$id_harga = $_POST['id_harga'];
		$id_pel = $_POST['id_pel'];
		$id_bbn = $_POST['id_bbn'];
		$masa_awal = $_POST['masa_awal'];
		$masa_akhir = $_POST['masa_akhir'];
		$volume = $_POST['volume'];
		$tagihan = $_POST['jumlah'];
		$total = $_POST['total'];
		$jmlh_tunggakan = $_POST['tung'];

		$data = array(
			'id_pel' => $id_pel,
			'id_harga' => $id_harga,
			'id_bbn' => $id_bbn,
			'masa_awal' => $masa_awal,
			'masa_akhir' => $masa_akhir,
			'volume' => $volume,
			'tagihan' => $tagihan,
			'total' => $total

		);

		$where = array(
			'id_trans' => $id_trans
		);

		$data = $this->models->update('transaksi', $data, $where);

		redirect(base_url('kettp'));
	}

	public function dodelktp($data){
		$data = array('id_trans' => $data);
		$this->models->delete('transaksi',$data);
		redirect(base_url('kettp'));
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	
	public function lap(){
		$this->load->view('header');
		$this->load->view('laporan');
		$this->load->view('footer');
	}

	public function cetaktung()	{
		$dompdf = new Dompdf();
		$data = $this->models->cetaktung();
		$data1 = $this->models->tung();
		$data = array(
			'data' => $data,
			'total' => $data1[0]['tunggakan']
		);

		$html = $this->load->view('cetaktung', $data, true);
		$dompdf->load_html($html);
		$dompdf->set_paper('A4', 'potrait');
		$dompdf->render();
		$pdf = $dompdf->output();
		$dompdf->stream('tunggakan.pdf', array('Attachment' => false));
	}

	public function cetaktahun(){
		$dompdf = new Dompdf();
		$tahun = $_POST['tahun'];
		$tag = $this->models->tag();
		$tung = $this->models->tung();
		$bay = $this->models->bay();
		$volume = $this->models->vol();
		$data = $this->models->cetaktahun($tahun);
		$data = array(
			'data' => $data,
			'tahun' => $tahun,
			'tunggakan' => $tung,
			'tagihan' => $tag,
			'bayar' => $bay,
			'volume' => $volume
		);

		$html = $this->load->view('cetaktahun', $data, true);
		$dompdf->load_html($html);
		$dompdf->set_paper('A4', 'landscape');
		$dompdf->render();
		$pdf = $dompdf->output();
		$dompdf->stream('cetaktahun.pdf', array('Attachment' => false));
	}

	public function cetakbln(){
		$dompdf = new Dompdf();
		$first = $_POST['first'];
		$last = $_POST['last'];
		$data = $this->models->cetakbln($first, $last);
		$data = array(
			'data' => $data
		);

		$html = $this->load->view('cetak', $data, true);
		$dompdf->load_html($html);
		$dompdf->set_paper('A4', 'landscape');
		$dompdf->render();
		$pdf = $dompdf->output();
		$dompdf->stream('cetak.pdf', array('Attachment' => false));
	}

	public function cetakhr(){
		$dompdf = new Dompdf();
		$first = $_POST['first'];
		$last = $_POST['last'];
		$data = $this->models->cetakhr($first, $last);
		$data = array(
			'data' => $data,
			'first' => $first,
			'last' => $last
		);

		$html = $this->load->view('cetakhari', $data, true);
		$dompdf->load_html($html);
		$dompdf->set_paper('A4', 'landscape');
		$dompdf->render();
		$pdf = $dompdf->output();
		$dompdf->stream('cetak.pdf', array('Attachment' => false));
	}

	public function print($id_trans){

		$data = $this->models->print($id_trans);
		$data  = array('id_trans' => $data[0]['id_trans'],
			'nama_pel' => $data[0]['nama_pel'],
			'alamat_pel' => $data[0]['alamat_pel'],
			'volume' => $data[0]['volume'],
			'masa_awal' => $data[0]['masa_awal'],
			'masa_akhir' => $data[0]['masa_akhir'],
			'tagihan' => $data[0]['tagihan'],
			'bayar' => $data[0]['bayar'],
			'total' => $data[0]['total'],
			'tgl_bayar' => $data[0]['tgl_bayar'],
			'jmlh_tunggakan' => $data[0]['jmlh_tunggakan'],
			'id_tung' => $data[0]['id_tung']

		);
		$res = $this->load->view('print', $data);
		redirect(base_url("kettp"));
	}

}