<?php
defined('BASEPATH') OR exit('No direct access allowed');

/**
 * 
 */
class Models extends CI_Model{
	
	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->where('username',$username)
		->where('password',md5($password))
		->get('user');

		if ($query->num_rows() > 0) {
			$data = array('username' => $username,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($data);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function data($table){
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function add($table,$data){
		$res = $this->db->insert($table,$data);
		return $res;

	}

	public function update($table,$data,$where){
		$res = $this->db->update($table,$data, $where);
		return $res;

	}

	public function getwhere($table,$data){
		$res = $this->db->get_where($table,$data);
		return $res->result_array();
	}

	public function delete($table,$where){
		$res = $this->db->delete($table,$where);
		return $res;
	}

	public function getses(){
	$id = $this->session->userdata['logged_in']['id_us']; // dapatkan id user yg login
	$this->db->select('id_us, nama_us');
	$this->db->where('id_us', $id);//
	$this->db->from('user');
	$query = $this->db->get();
	return $query->result();
}

public function datpem(){
	$this->db->select('transaksi.id_trans, pelanggan.nama_pel, transaksi.masa_awal, transaksi.masa_akhir, transaksi.tagihan, transaksi.volume, transaksi.tgl_bayar, tunggakan.jmlh_tunggakan, transaksi.bayar');
	$this->db->from('pelanggan');
	$this->db->join('transaksi','transaksi.id_pel = pelanggan.id_pel');
	$this->db->join('tunggakan', 'tunggakan.id_pel = transaksi.id_pel');
	$this->db->order_by('transaksi.id_trans', 'desc');
	$query = $this->db->get();
	return $query->result_array();
}

public function pem($id_trans){
	$this->db->select('transaksi.id_trans, pelanggan.nama_pel, pelanggan.alamat_pel, transaksi.masa_awal, transaksi.masa_akhir, transaksi.tagihan, transaksi.volume, tunggakan.jmlh_tunggakan, transaksi.bayar, tunggakan.id_tung');
	$this->db->from('pelanggan');
	$this->db->join('transaksi','transaksi.id_pel = pelanggan.id_pel');
	$this->db->join('tunggakan', 'tunggakan.id_pel = transaksi.id_pel');
	$this->db->where('transaksi.id_trans =',$id_trans);
	$query = $this->db->get();
	return $query->result_array();
}

public function print($id_trans){
	$this->db->select('transaksi.id_trans, pelanggan.nama_pel, pelanggan.alamat_pel, transaksi.masa_awal, transaksi.masa_akhir, transaksi.tagihan, transaksi.volume, tunggakan.jmlh_tunggakan, transaksi.bayar, tunggakan.id_tung, transaksi.tgl_bayar, transaksi.total');
	$this->db->from('pelanggan');
	$this->db->join('transaksi','transaksi.id_pel = pelanggan.id_pel');
	$this->db->join('tunggakan', 'tunggakan.id_pel = transaksi.id_pel');
	$this->db->where('transaksi.id_trans =',$id_trans);
	$query = $this->db->get();
	return $query->result_array();
}

public function pel(){
	$this->db->select('count(pelanggan.id_pel) as pelanggan');
	$this->db->from('pelanggan');
	$query = $this->db->get();
	return $query->result_array();
}

public function vol(){
	$this->db->select('sum(transaksi.volume) as volume');
	$this->db->from('transaksi');
	$query = $this->db->get();
	return $query->result_array();
}

public function tag(){
	$this->db->select('sum(transaksi.tagihan) as tagihan');
	$this->db->from('transaksi');
	$query = $this->db->get();
	return $query->result_array();
}

public function bay(){
	$this->db->select('sum(transaksi.bayar) as bayar');
	$this->db->from('transaksi');
	$query = $this->db->get();
	return $query->result_array();
}

public function tung(){
	$this->db->select('sum(tunggakan.jmlh_tunggakan) as tunggakan');
	$this->db->from('tunggakan');
	$query = $this->db->get();
	return $query->result_array();
}

public function cetaktung(){
	$this->db->select('pelanggan.nama_pel, pelanggan.alamat_pel, tunggakan.jmlh_tunggakan');
	$this->db->from('tunggakan');
	$this->db->join('pelanggan','pelanggan.id_pel = tunggakan.id_pel');
	$this->db->order_by('tunggakan.jmlh_tunggakan', 'desc');
	$query = $this->db->get();
	return $query->result_array();
}

public function cetaktahun($tahun){
	$this->db->select('pelanggan.nama_pel, pelanggan.alamat_pel, sum(transaksi.volume) as volume, sum(transaksi.tagihan) as tagihan, sum(transaksi.bayar) as bayar, tunggakan.jmlh_tunggakan');
	$this->db->from('tunggakan');
	$this->db->join('transaksi', 'transaksi.id_pel = tunggakan.id_pel');
	$this->db->join('pelanggan', 'pelanggan.id_pel = transaksi.id_pel');
	$this->db->where('masa_awal >=', $tahun.'-01-01');
	$this->db->where('masa_awal <=', $tahun.'-12-01');
	$this->db->group_by('pelanggan.nama_pel');
	$this->db->group_by('pelanggan.alamat_pel');
	$query = $this->db->get();
	return $query->result_array();
}

public function cetakbln($first, $last){
	$this->db->select('pelanggan.nama_pel, pelanggan.alamat_pel, transaksi.volume, transaksi.tagihan, transaksi.bayar, tunggakan.jmlh_tunggakan, transaksi.tgl_bayar, transaksi.masa_awal, transaksi.masa_akhir');
	$this->db->from('tunggakan');
	$this->db->join('transaksi', 'transaksi.id_pel = tunggakan.id_pel');
	$this->db->join('pelanggan', 'pelanggan.id_pel = transaksi.id_pel');
	$this->db->where('masa_awal >=', $first);
	$this->db->where('masa_awal <=', $last);
	$this->db->order_by('pelanggan.nama_pel');
	$this->db->order_by('masa_awal','asc');
	$query = $this->db->get();
	return $query->result_array();
}

public function cetakhr($first, $last){
	$this->db->select('pelanggan.nama_pel, pelanggan.alamat_pel, transaksi.volume, transaksi.tagihan, transaksi.bayar, tunggakan.jmlh_tunggakan, transaksi.tgl_bayar, transaksi.masa_awal, transaksi.masa_akhir');
	$this->db->from('tunggakan');
	$this->db->join('transaksi', 'transaksi.id_pel = tunggakan.id_pel');
	$this->db->join('pelanggan', 'pelanggan.id_pel = transaksi.id_pel');
	$this->db->where('tgl_bayar >=', $first);
	$this->db->where('tgl_bayar <=', $last);
	$this->db->order_by('pelanggan.nama_pel');
	$query = $this->db->get();
	return $query->result_array();
}

}