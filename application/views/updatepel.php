                
<br>
<div class="login-form-area mg-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="login-bg animated zoomInDown shadow-reset">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-sparkline12-hd">
                                <h1>Update Data Pelanggan</h1>
                            </div>
                        </div>
                        <br>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <form id="submit" method="POST" action="<?php echo base_url()."update"?>" >
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">ID Pelanggan</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="id_pel" required="" readonly="true" value="<?php echo $id_pel ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Nama Pelanggan</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="nama_pel" required="" value="<?php echo $nama_pel ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Alamat</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="alamat_pel" required="" value="<?php echo $alamat_pel ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-2 ">
                                                        <div class="button-style-three">
                                                            <button class="btn btn-custon-rounded-three btn-primary" type="submit">Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



