<?php
defined('BASEPATH') OR exit('No direct access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/Admin/plugins/bootstrap/css/bootstrap.css'); ?>" >

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/Admin/plugins/node-waves/waves.css'); ?>" >

    <!-- Animation Css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/Admin/plugins/animate-css/animate.css'); ?>" >

    <!-- Custom Css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/Admin/css/style.css'); ?>" >
</head>

<?php 
if (!empty($notif)) {
    echo '<div class = "alert alert-danger">';
    echo $notif;
    echo '</div>';
}
?>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">SIM <b>PAMDES</b></a>
            <small>Desa Kelor, Kecamatan Karangmojo</small>
        </div>
        <div class="card">
            <div class="body">
                <form  method = "post" action="<?php echo base_url()."dologin"?> ">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                            <!-- <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span> -->
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <!-- <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span> -->
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <button type="submit" name="submit" value="Login" class="btn btn-block bg-pink waves-effect"> Login </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Jquery Core Js -->
        <script src="<?php echo base_url('assets/Admin/plugins/jquery/jquery.min.js'); ?>"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?php echo base_url('assets/Admin/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?php echo base_url('assets/Admin/plugins/node-waves/waves.js'); ?>"></script>

        <!-- Validation Plugin Js -->
        <script src="<?php echo base_url('assets/Admin/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

        <!-- Custom Js -->
        <script src="<?php echo base_url('assets/Admin/js/admin.js'); ?>"></script>
        <script src="<?php echo base_url('assets/Admin/js/pages/examples/sign-in.js'); ?>"></script>
    </body>

    </html>