<?php
require_once(APPPATH.'vendor/mike42/escpos-php/autoload.php');
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
// use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

// $connector = null;
// $connector = new NetworkPrintConnector("192.168.10.223", 9100);
$connector = new WindowsPrintConnector("HP LaserJet M1530 MFP Series PCL 6 penetapan");
$printer = new Printer($connector);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text("Struk Pembayaran\n");
$printer -> text("TIRTA RAHAYU\n");
$testStr = ($id_trans);
$printer -> feed();
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> text("No. Transaksi : ".$id_trans."\n");
$printer -> text("Nama          : ".$nama_pel."\n");
$printer -> text("Alamat        : ".$alamat_pel."\n");
$printer -> text("Bulan         : ".$masa_awal .'-'. $masa_akhir."\n");
$printer -> text("Volume        : ".number_format($volume,0)."\n");
$printer -> text("Tagihan       : ".number_format($tagihan,0)."\n");
$printer -> text("Total Tagihan : ".number_format($total,0)."\n");
$printer -> text("Bayar         : ".number_format($bayar,0)."\n");
$printer -> text("Sisa Tunggakan: ".number_format($jmlh_tunggakan,0)."\n");
$printer -> text("Tanggal Bayar : ".$tgl_bayar."\n");
$printer -> selectPrintMode();
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text("\n");
$printer -> text("SIMPANLAH BUKTI PEMBAYARAN\n");
$printer -> text("SEBAGAI PEMBAYARAN YANG SAH\n");
$printer -> setFont(Printer::FONT_B);
$printer -> text("@Mervia@");
$printer -> feed();
$printer->cut();
$printer->close();
?>
