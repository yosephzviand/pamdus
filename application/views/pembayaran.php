<br>
<div class="row">
    <div class="col-lg-12">
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sparkline13-list  animated zoomInDown shadow-reset">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Data <span class="table-project-n">Pembayaran</span></h1>
                                    <div class="sparkline13-outline-icon">
                                        <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <div id="toolbar">
                                        <select class="form-control">
                                            <option value="">Export Basic</option>
                                            <option value="all">Export All</option>
                                            <option value="selected">Export Selected</option>
                                        </select>
                                    </div>
                                    <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                        <thead>
                                            <tr>
                                                <th data-field="id">NO</th>
                                                <th data-field="id_trans" data-editable="true">ID Transaksi</th>
                                                <th data-field="nama" data-editable="true">Nama</th>
                                                <th data-field="bulan" data-editable="true">Bulan</th>
                                                <th data-field="volume" data-editable="true">Volume m3</th>
                                                <th data-field="tagihan" data-editable=" true">Tagihan</th>
                                                <th data-field="tunggakan" data-editable="true">Tunggakan</th>
                                                <th data-field="tglbayar" data-editable="true">Tanggal Bayar</th>
                                                <th data-field="action">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach($data as $data): ?>
                                                <tr>
                                                    <td><?php echo $i++;?></td>
                                                    <td><?php echo $data['id_trans'] ?></td>
                                                    <td><?php echo $data['nama_pel'] ?></td>
                                                    <td><?php echo $data['masa_awal']." - ".$data['masa_akhir'] ?></td>
                                                    <td><?php echo $data['volume'] ?></td>
                                                    <td><?php echo number_format($data['tagihan'],0); ?></td>
                                                    <td><?php echo number_format($data['jmlh_tunggakan'],0); ?></td>
                                                    <td><?php echo $data['tgl_bayar'] ?></td>
                                                    <td>
                                                        <div class="button-style-three"> 
                                                         
                                                            <a href="<?php echo base_url()."controls/bayar/".$data['id_trans'] ?>">
                                                               <?php if ($data['tgl_bayar'] == 0) { ?>
                                                                <input type="button" class="btn btn-custon-rounded-three btn-primary btn-xs" value="Bayar">
                                                            </a>
                                                        <?php } elseif($data['tgl_bayar'] > 0) { ?>
                                                            <a href="<?php echo base_url()."controls/btlbyr/".$data['id_trans'] ?>" onclick="return confirm('Yakin Akan Di Hapus?')">
                                                                <input type="button" class="btn btn-custon-rounded-three btn-danger btn-xs" value="Hapus">
                                                            </a>
                                                        <?php }?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>