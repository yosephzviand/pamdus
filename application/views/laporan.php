<br>
<div class="adminpro-accordion-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="admin-pro-accordion-wrap mg-b-15 animated zoomInDown shadow-reset">
                    <div class="alert-title">
                        <h2>Pembukuan dan Pelaporan</h2>
                        <!-- <p>These are the Custom bootstrap Animate bounce Accordion style 1</p> -->
                    </div>
                    <br>
                    <div class="panel-group adminpro-custon-design" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-head">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    Laporan Pembayaran / Hari</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse panel-ic collapse">
                                <div class="panel-body admin-panel-content animated bounce">
                                    <form method="POST" action="<?php echo base_url()."cetakhr" ?>" target="_blank">
                                        <div class="date-picker-inner">
                                            <div class="form-group data-custon-pick data-custom-mg" id="data_5">
                                                <label>Range Tanggal</label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <input type="date" class="form-control" name="first" required autofocus />
                                                    <span class="input-group-addon">to</span>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <input type="date" class="form-control" name="last" required autofocus  />
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <button class="btn btn-custon-rounded-three btn-primary" type="submit">Cetak</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-head">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    Laporan Pembayaran / Bulan</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse panel-ic collapse">
                                <div class="panel-body admin-panel-content animated bounce">
                                    <form method="POST" action="<?php echo base_url()."cetakbln" ?>" target="_blank">
                                        <div class="date-picker-inner">
                                            <div class="form-group data-custon-pick data-custom-mg" id="data_5">
                                                <label>Range Bulan</label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <input type="date" class="form-control" name="first" required autofocus />
                                                    <span class="input-group-addon">to</span>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <input type="date" class="form-control" name="last" required autofocus  />
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <button class="btn btn-custon-rounded-three btn-primary" type="submit">Cetak</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-head">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    Laporan Pembayaran / Tahun</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse panel-ic collapse">
                                <div class="panel-body admin-panel-content animated bounce">
                                    <form method="POST" action="<?php echo base_url()."cetaktahun" ?>" target="_blank">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Masukkan Tahun</label>
                                                <div class="input-mark-inner">
                                                    <input type="text" id="tahun" name="tahun" class="form-control" data-mask="9999" required="true" placeholder="">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <br>
                                        <button class="btn btn-custon-rounded-three btn-primary" type="submit">Cetak</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-head">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    Laporan Tunggakan</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse panel-ic collapse">
                                <div class="panel-body admin-panel-content animated bounce">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <a href="<?php echo base_url()."cetaktung" ?>" target="_blank"> Cetak Tunggakan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- accordion End-->
</div>
</div>