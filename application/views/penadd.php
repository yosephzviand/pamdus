            <br>
            <div class="login-form-area mg-t-30 mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div class="login-bg animated zoomInDown shadow-reset">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-sparkline12-hd">
                                            <h1>Data Pelanggan</h1>
                                        </div>
                                    </div>
                                    <br>
                                    <form method="POST" action="<?php echo base_url()."trans"?> ">
                                        <div class="sparkline12-graph">
                                            <div class="basic-login-form-ad">
                                                <div class="row">
                                                    <div class="col-lg-10">
                                                        <div class="all-form-element-inner">
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">ID </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="id_pel" readonly="true" class="form-control" value="<?php echo $id_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Nama </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="nama_pel" readonly="true" class="form-control" value="<?php echo $nama_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Alamat</label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="alamat_pel" readonly="true" class="form-control" value="<?php echo $alamat_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="main-sparkline12-hd">
                                                <h1>Data Penetapan</h1>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="sparkline12-graph">
                                            <div class="basic-login-form-ad">
                                                <div class="row">
                                                    <div class="col-lg-10">
                                                        <div class="all-form-element-inner">
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Bulan </label>
                                                                    </div>
                                                                    <div class="col-lg-5">
                                                                        <div class="form-group data-custon-pick data-custom-mg">
                                                                            <div class="input-daterange input-group" id="datepicker">
                                                                                <span class="input-group-addon">
                                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                                <input type="date" class="form-control" required="" name="masa_awal" autofocus />
                                                                                <span class="input-group-addon">to</span>
                                                                                <span class="input-group-addon">
                                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                                <input type="date" class="form-control" required="" name="masa_akhir" autofocus  />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Golongan Harga </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <select id="golongan" name="golongan" class="golongan form-control show-tick" required="" autofocus>
                                                                            <option value="">-- Please select --</option>
                                                                            <?php foreach ($data as $data) : ?>
                                                                                <option value="<?php echo $data['id_harga']?>" data-city="<?php echo $data['harga'] ?>"> <?php echo $data['nama_harga']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Volume </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="volume" required="" name="volume" class="form-control" placeholder="Volume m3" onkeyup="sum();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="id_harga" name="id_harga" readonly="true" class="form-control" />
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Harga </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="harga" name="harga" readonly="true" class="form-control" placeholder="Harga m3" onkeyup="sum();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="id_bbn" name="id_bbn" readonly="true" class="form-control" value="<?php echo $id_bbn ?>" />
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Beban </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="beban" name="beban" value="<?php echo $harga_bbn ?>"  readonly="true" class="form-control" placeholder="Beban" onkeyup="sum();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro"> Tagihan </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="jumlah" name="jumlah"  readonly="true" class="form-control" onkeyup="sum();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="id_tung" name="id_tung" readonly="true" class="form-control" value="<?php  
                                                            if (empty($data1[0]['id_tung'])){
                                                                                // echo null;
                                                                } else {
                                                                    echo $data1[0]['id_tung'];
                                                                } ?>" />
                                                                <div class="form-group-inner">
                                                                    <div class="row">
                                                                        <div class="col-lg-3">
                                                                            <label class="login2 pull-right pull-right-pro"> Tunggakan </label>
                                                                        </div>
                                                                        <div class="col-lg-8">
                                                                            <input type="text" id="tung" name="tung"  readonly="true" class="form-control" value="<?php 
                                                                            if (empty($data1[0]['id_tung'])){
                                                                                echo 0;
                                                                                } else {
                                                                                    echo $data1[0]['jmlh_tunggakan'];
                                                                                } ?>" onkeyup="sum();"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label class="login2 pull-right pull-right-pro">Total Tagihan </label>
                                                                            </div>
                                                                            <div class="col-lg-8">
                                                                                <input type="text" id="total" name="total"  readonly="true" class="form-control" onkeyup="sum();"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-xs-2 ">
                                                                <div class="button-style-three">
                                                                    <button class="btn btn-custon-rounded-three btn-primary" type="submit">Simpan</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        