<div class="main-sparkline10-hd" align="center">
	<h2>Laporan Bulanan</h2>
</div>
<br>
<hr>
<br>
<br>
<table border="1" class="table border-table" width="100%">
	<thead align="center">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Bulan</th>
			<th>Volume m3</th>
			<th>Jumlah Tagihan</th>
			<th>Jumlah Bayar</th>
			<th>Tunggakan</th>
			<th>Tanggal Bayar</th>
		</tr>
	</thead>
	<tbody >
		<?php $i = 1; ?>
		<?php foreach($data as $data): ?>
			<tr>
				<td align="center"><?php echo $i++;?></td>
				<td><?php echo $data['nama_pel'] ?></td>
				<td><?php echo $data['alamat_pel'] ?></td>
				<td><?php echo $data['masa_awal'].' s/d '.$data['masa_akhir'] ?></td>
				<td align="right"><?php echo $data['volume']; ?></td>
				<td align="right"><?php echo number_format($data['tagihan'],0); ?></td>
				<td align="right"><?php echo number_format($data['bayar'],0); ?></td>
				<td align="right"><?php echo number_format($data['jmlh_tunggakan'],0); ?></td>
				<td align="center"><?php echo $data['tgl_bayar']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>