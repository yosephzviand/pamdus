
<br>
<div class="login-form-area mg-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="login-bg animated zoomInDown shadow-reset">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-sparkline12-hd">
                                <h1>Input Data Pengguna</h1>
                            </div>
                        </div>
                        <br>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <form id="submit" method="POST" action="<?php echo base_url()."insertusr"?>" >
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro" >Nama</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" required="" name="nama_us" required="" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Username</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" required="" name="username" required="" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Password</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="Password" required="" name="password" required="" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-2 ">
                                                        <div class="button-style-three">
                                                            <button class="btn btn-custon-rounded-three btn-primary" type="submit">Tambah</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



