


<!-- Chat Box End-->
    <!-- jquery
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/vendor/jquery-1.11.3.min.js'); ?>"></script>
    <!-- bootstrap JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/bootstrap.min.js'); ?>"></script>
    <!-- meanmenu JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/jquery.meanmenu.js'); ?>"></script>
    <!-- mCustomScrollbar JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
    <!-- sticky JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/jquery.sticky.js'); ?>"></script>
    <!-- scrollUp JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/jquery.scrollUp.min.js'); ?>"></script>
    <!-- scrollUp JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/wow/wow.min.js'); ?>"></script>
    <!-- colorpicker JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/colorpicker/jquery.spectrum.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/colorpicker/color-picker-active.js'); ?>"></script>
    <!-- datapicker JS
      ============================================ -->
      <script src="js/datapicker/bootstrap-datepicker.js"></script>
      <script src="js/datapicker/datepicker-active.js"></script>
    <!-- counterup JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/counterup/jquery.counterup.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/counterup/waypoints.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/counterup/counterup-active.js'); ?>"></script>
    <!-- jvectormap JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/jvectormap/jquery-jvectormap-2.0.2.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/jvectormap/jvectormap-active.js'); ?>"></script>
    <!-- peity JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/peity/jquery.peity.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/peity/peity-active.js'); ?>"></script>
    <!-- sparkline JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/sparkline/jquery.sparkline.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/sparkline/sparkline-active.js'); ?>"></script>
     <!-- input-mask JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/input-mask/jasny-bootstrap.min.js'); ?>"></script>
    <!-- flot JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.tooltip.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.spline.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.resize.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.pie.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.symbol.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/jquery.flot.time.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/flot/dashtwo-flot-active.js'); ?>"></script>
    <!-- data table JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/tableExport.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/data-table-active.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-editable.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/bootstrap-editable.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-resizable.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/colResizable-1.5.source.js'); ?>"></script>
      <script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-export.js'); ?>"></script>
    <!-- main JS
      ============================================ -->
      <script src="<?php echo base_url('assets/material/js/main.js'); ?>"></script>


      <script type="text/javascript">

        $("#golongan").on("change", function(){

      // ambil nilai
      var harga = $("#golongan option:selected").data("city");
      var value = $("#golongan option:selected").val();
      
      // pindahkan nilai ke input
      $("#harga").val(harga);
      $('#id_harga').val(value);
      
    });

  </script>

  <script>
    function sum() {
      var txtFirstNumberValue = document.getElementById('volume').value;
      var txtSecondNumberValue = document.getElementById('harga').value;
      var txtThreNumberValue = document.getElementById('beban').value;

      var result = parseFloat(txtFirstNumberValue) * parseInt(txtSecondNumberValue) + parseInt(txtThreNumberValue);
      if (!isNaN(result)) {
       document.getElementById('jumlah').value = result;
     }

     var tung = document.getElementById('tung').value;
     var result1 = parseInt(result) + parseInt(tung);
     if (!isNaN(result1)) {
      document.getElementById('total').value = result1;
    }
  }
</script>

<script type="text/javascript">
  function sum1(){

    var tot = document.getElementById('total').value;
    var bay = document.getElementById('bayar').value;
    var result2 = parseInt(tot) - parseInt(bay);
    if (!isNaN(result2)) {
      document.getElementById('tagihan').value = result2;
    }
  }
</script>

<script type="text/javascript">
  function sum2(){
    var jum = document.getElementById('jumlah').value;
    var tung = document.getElementById('tung').value;
    var bay = document.getElementById('bayar').value;
    var result1 = parseInt(bay) + parseInt(tung) - parseInt(jum);
    if (!isNaN(result1)) {
      document.getElementById('tagihan').value = result1;
    }
  }
</script>


</body>

</html>
