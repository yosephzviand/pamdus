
    <!-- Google Fonts
    	============================================ -->
    	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/bootstrap.min.css'); ?>">
    <!-- Bootstrap CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/font-awesome.min.css'); ?>">
    <!-- adminpro icon CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/adminpro-custon-icon.css'); ?>">
    <!-- meanmenu icon CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/meanmenu.min.css'); ?>">
    <!-- mCustomScrollbar CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/jquery.mCustomScrollbar.min.css'); ?>">
    <!-- animate CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/animate.css'); ?>">
    <!-- jvectormap CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/jvectormap/jquery-jvectormap-2.0.3.css'); ?>">
    <!-- normalize CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/data-table/bootstrap-table.css'); ?>">
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/data-table/bootstrap-editable.css'); ?>">
    <!-- accordions CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/accordions.css'); ?>">
    <!-- normalize CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/normalize.css'); ?>">
    <!-- colorpicker CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/colorpicker/colorpicker.css'); ?>">
     <!-- datapicker CSS
     	============================================ -->
     	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/datapicker/datepicker3.css'); ?>">
    <!-- buttons CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/buttons.css'); ?>">
    <!-- style CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/form.css'); ?>">
    <!-- charts CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/c3.min.css'); ?>">
    <!-- style CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/style.css'); ?>">
    <!-- responsive CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?php echo base_url('assets/material/css/responsive.css'); ?>">
    <!-- modernizr JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>


    	<div class="main-sparkline10-hd" align="center">
    		<h2>Laporan Tahun <?php echo $tahun ?></h2>
    	</div>
    	<br>
    	<hr>
    	<br>
    	<div class="datatable-dashv1-list custom-datatable-overright">
    		<table id="table" data-toggle="table">
    			<thead align="center">
    				<tr>
    					<th>No</th>
    					<th>Nama</th>
    					<th>Alamat</th>
    					<th>Volume m3</th>
    					<th>Jumlah Tagihan</th>
    					<th>Jumlah Bayar</th>
    					<th>Tunggakan</th>
    				</tr>
    			</thead>
    			<tbody >
    				<?php $i = 1; ?>
    				<?php foreach($data as $data): ?>
    					<tr>
    						<td align="center"><?php echo $i++;?></td>
    						<td><?php echo $data['nama_pel'] ?></td>
    						<td><?php echo $data['alamat_pel'] ?></td>
    						<td align="right"><?php echo $data['volume']; ?></td>
    						<td align="right"><?php echo number_format($data['tagihan'],0); ?></td>
    						<td align="right"><?php echo number_format($data['bayar'],0); ?></td>
    						<td align="right"><?php echo number_format($data['jmlh_tunggakan'],0); ?></td>
    					</tr>
    				<?php endforeach; ?>
    			</tbody>
    			<tr>
    				<td colspan="3" align="center"><b> Jumlah</b></td>
    				<td align="right"><b> <?php echo number_format($volume[0]['volume'],0) ?></b></td>
    				<td align="right"><b> <?php echo number_format($tagihan[0]['tagihan'],0) ?></b></td>
    				<td align="right"><b> <?php echo number_format($bayar[0]['bayar'],0) ?></b></td>
    				<td align="right"><b> <?php echo number_format($tunggakan[0]['tunggakan'],0) ?></b></td>
    			</tr>
    		</table>
    	</div>

    	<!-- Chat Box End-->
    <!-- jquery
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/vendor/jquery-1.11.3.min.js'); ?>"></script>
    <!-- bootstrap JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/bootstrap.min.js'); ?>"></script>
    <!-- meanmenu JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/jquery.meanmenu.js'); ?>"></script>
    <!-- mCustomScrollbar JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
    <!-- sticky JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/jquery.sticky.js'); ?>"></script>
    <!-- scrollUp JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/jquery.scrollUp.min.js'); ?>"></script>
    <!-- scrollUp JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/wow/wow.min.js'); ?>"></script>
    <!-- colorpicker JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/colorpicker/jquery.spectrum.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/colorpicker/color-picker-active.js'); ?>"></script>
    <!-- datapicker JS
    	============================================ -->
    	<script src="js/datapicker/bootstrap-datepicker.js"></script>
    	<script src="js/datapicker/datepicker-active.js"></script>
    <!-- counterup JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/counterup/jquery.counterup.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/counterup/waypoints.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/counterup/counterup-active.js'); ?>"></script>
    <!-- jvectormap JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/jvectormap/jquery-jvectormap-2.0.2.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/jvectormap/jvectormap-active.js'); ?>"></script>
    <!-- peity JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/peity/jquery.peity.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/peity/peity-active.js'); ?>"></script>
    <!-- sparkline JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/sparkline/jquery.sparkline.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/sparkline/sparkline-active.js'); ?>"></script>
     <!-- input-mask JS
     	============================================ -->
     	<script src="<?php echo base_url('assets/material/js/input-mask/jasny-bootstrap.min.js'); ?>"></script>
    <!-- flot JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.tooltip.min.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.spline.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.resize.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.pie.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.symbol.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/jquery.flot.time.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/flot/dashtwo-flot-active.js'); ?>"></script>
    <!-- data table JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/tableExport.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/data-table-active.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-editable.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/bootstrap-editable.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-resizable.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/colResizable-1.5.source.js'); ?>"></script>
    	<script src="<?php echo base_url('assets/material/js/data-table/bootstrap-table-export.js'); ?>"></script>
    <!-- main JS
    	============================================ -->
    	<script src="<?php echo base_url('assets/material/js/main.js'); ?>"></script>