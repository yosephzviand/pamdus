            <br>
            <div class="login-form-area mg-t-30 mg-b-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div class="login-bg animated zoomInDown shadow-reset">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-sparkline12-hd">
                                            <h1>Data Pelanggan</h1>
                                        </div>
                                    </div>
                                    <br>
                                    <form method="POST" action="<?php echo base_url()."hapuskettp"?>">
                                        <div class="sparkline12-graph">
                                            <div class="basic-login-form-ad">
                                                <div class="row">
                                                    <div class="col-lg-10">
                                                        <div class="all-form-element-inner">
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">ID </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="id_pel" readonly="true" class="form-control" value="<?php echo $id_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Nama </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="nama_pel" readonly="true" class="form-control" value="<?php echo $nama_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro">Alamat</label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" name="alamat_pel" readonly="true" class="form-control" value="<?php echo $alamat_pel ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="main-sparkline12-hd">
                                                <h1>Data Penetapan</h1>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="sparkline12-graph">
                                            <div class="basic-login-form-ad">
                                                <div class="row">
                                                    <div class="col-lg-10">
                                                        <div class="all-form-element-inner">
                                                            <input type="hidden" id="id_trans" name="id_trans" readonly="true" class="form-control" value="<?php echo $id_trans ?>" />

                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro"> Tagihan </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="jumlah" name="jumlah" value="<?php echo $tagihan ?>"  readonly="true" class="form-control" onkeyup="sum2();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="id_tung" name="id_tung" readonly="true" class="form-control" value="<?php echo $id_tung ?>"/>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro"> Tunggakan </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="tung" name="tung"  readonly="true" class="form-control" value="<?php echo $jmlh_tunggakan ?>" onkeyup="sum2();"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro"> Bayar </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="bayar" name="bayar" value="<?php 
                                                                        if (empty($bayar)){
                                                                            echo 0;
                                                                            } else {
                                                                               echo $bayar;
                                                                           }
                                                                           ?>" class="form-control" readonly="" onkeyup="sum2();"/>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                               <div class="form-group-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="login2 pull-right pull-right-pro"> Sisa Tunggakan </label>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <input type="text" id="tagihan" name="tagihan"  class="form-control" onkeyup="sum2();">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-xs-2 ">
                                                        <div class="button-style-three">
                                                            <button class="btn btn-custon-rounded-three btn-primary" type="submit">Hapus</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
