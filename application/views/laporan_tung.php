
<div class="main-sparkline10-hd" align="center">
    <h2>Laporan Tunggakan</h2>
</div>
<br>
<div class="sparkline10-graph">
    <div class="static-table-list">
        <br>
        <table border="1" class="table border-table" width="100%">
            <thead align="center">
                <tr>
                   <th >No</th>
                   <th >Nama</th>
                   <th >Alamat</th>
                   <th >Tunggakan</th>
               </tr>
           </thead>
           <tbody >
            <?php $i = 1; ?>
            <?php foreach($data as $data): ?>
              <tr>
                  <td align="center"><?php echo $i++;?></td>
                  <td><?php echo $data['nama_pel'] ?></td>
                  <td><?php echo $data['alamat_pel'] ?></td>
                  <td align="right"><?php echo number_format($data['jmlh_tunggakan'],0); ?></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
      <tr>
       <td colspan="3" align="center"><b> Jumlah</b></td>
       <td align="right"><b> <?php echo number_format($total,0) ?></b></td>
   </tr>
</table>
</div>
</div>