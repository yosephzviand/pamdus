                
<br>
<div class="login-form-area mg-t-30 mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="login-bg animated zoomInDown shadow-reset">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-sparkline12-hd">
                                <h1>Pembayaran</h1>
                            </div>
                        </div>
                        <br>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <form id="submit" method="POST" action="<?php echo base_url()."updatebayar"?> " >
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Id Transaksi</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="id_trans" readonly="true" class="form-control" value="<?php echo $id_trans ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="id_pel" name="id_pel" readonly="true" class="form-control" value="<?php echo $id_pel ?>" />
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Nama Pelanggan</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="nama_pel" readonly="true" class="form-control" value="<?php echo $nama_pel ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Alamat</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="alamat_pel" readonly="true" class="form-control" value="<?php echo $alamat_pel ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Bulan</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="masa" class="form-control" readonly="true" value="<?php echo $masa_awal." - ".$masa_akhir ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Volume</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="volume" class="form-control" readonly="true" value="<?php echo $volume ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Tagihan</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" name="jumlah" class="form-control" readonly="true" value="<?php echo $tagihan ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="id_tung" name="id_tung" readonly="true" class="form-control" value="<?php  
                                                if (empty($data1[0]['id_tung'])){
                                                                                // echo null;
                                                    } else {
                                                        echo $data1[0]['id_tung'];
                                                    } ?>" />
                                                    <div class="form-group-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <label class="login2 pull-right pull-right-pro"> Tunggakan </label>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <input type="text" id="tung" name="tung"  readonly="true" class="form-control" value="<?php 
                                                                if (empty($data1[0]['id_tung'])){
                                                                    echo 0;
                                                                    } else {
                                                                        echo $data1[0]['jmlh_tunggakan'];
                                                                    } ?>" onkeyup="sum();"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro">Total Tagihan </label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" id="total" name="total" value="<?php echo $total ?>" readonly="true" class="form-control" onkeyup="sum1();"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro"> Bayar </label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" id="bayar" required="" name="bayar" class="form-control" onkeyup="sum1();" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-3">
                                                                    <label class="login2 pull-right pull-right-pro"> Sisa Tunggakan </label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" id="tagihan" name="tagihan"  readonly="true" class="form-control" onkeyup="sum1();">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-2 ">
                                                                <div class="button-style-three">
                                                                    <button class="btn btn-custon-rounded-three btn-primary" type="submit">Bayar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




