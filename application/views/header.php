<?php
defined('BASEPATH') OR exit('No direct access allowed');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SIMPAMDUS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/bootstrap.min.css'); ?>">
    <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/font-awesome.min.css'); ?>">
    <!-- adminpro icon CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/adminpro-custon-icon.css'); ?>">
    <!-- meanmenu icon CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/meanmenu.min.css'); ?>">
    <!-- mCustomScrollbar CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/jquery.mCustomScrollbar.min.css'); ?>">
    <!-- animate CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/animate.css'); ?>">
    <!-- jvectormap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/jvectormap/jquery-jvectormap-2.0.3.css'); ?>">
    <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/data-table/bootstrap-table.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/data-table/bootstrap-editable.css'); ?>">
    <!-- accordions CSS
       ============================================ -->
       <link rel="stylesheet" href="<?php echo base_url('assets/material/css/accordions.css'); ?>">
    <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/normalize.css'); ?>">
    <!-- colorpicker CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/colorpicker/colorpicker.css'); ?>">
     <!-- datapicker CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/datapicker/datepicker3.css'); ?>">
    <!-- buttons CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/buttons.css'); ?>">
    <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/form.css'); ?>">
    <!-- charts CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/c3.min.css'); ?>">
    <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/style.css'); ?>">
    <!-- responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/material/css/responsive.css'); ?>">
    <!-- modernizr JS
        ============================================ -->
        <script src="<?php echo base_url('assets/material/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
    </head>

    <body class="materialdesign">
        <div class="wrapper-pro">
            <div class="left-sidebar-pro">
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <h3><b>SIM PAMDUS</b></h3>
                        <p>TIRTA RAHAYU</p>
                        <strong>AP+</strong>
                    </div>
                    <div class="left-custom-menu-adp-wrap">
                        <ul class="nav navbar-nav left-sidebar-menu-pro">
                            <li class="nav-item">
                                <a href="<?php echo base_url()."dashboard" ?>" role="button" aria-expanded="false" ><i class="fa big-icon fa-home"></i> <span class="mini-dn">Dashboard</span> </a>
                            </li>
                            
                            <li class="nav-item"><a href="<?php echo base_url()."dodata"?>" role="button" aria-expanded="false" ><i class="fa big-icon fa-child"></i> <span class="mini-dn">Daftar Pelanggan</span></a>
                            </li>

                            <li class="nav-item"><a href="<?php echo base_url()."dogol" ?>" role="button" aria-expanded="false" ><i class="fa big-icon fa-flask"></i> <span class="mini-dn">Tarif Golongan</span></a>
                            </li>

                            <li class="nav-item"><a href="<?php echo base_url()."dobbn" ?>" role="button" aria-expanded="false"><i class="fa big-icon fa-pie-chart"></i> <span class="mini-dn">Biaya Beban</span> </a>
                            </li>

                            <li class="nav-item"><a href="<?php echo base_url()."pen" ?>" role="button" aria-expanded="false" ><i class="fa big-icon fa-bar-chart-o"></i> <span class="mini-dn">Transaksi Pembayaran </span> </a>
                            </li>

                            <li class="nav-item"><a href="<?php echo base_url()."kettp" ?>" role="button" aria-expanded="false"> <i class="fa big-icon fa-table"></i> <span class="mini-dn">Daftar Tagihan</span></a>
                            </li>
                            <li class="nav-item"><a href="<?php echo base_url()."lap" ?>"  role="button" aria-expanded="false"><i class="fa big-icon fa-edit"></i> <span class="mini-dn">Laporan </span></a>
                            </li>
                            <li class="nav-item"><a href="<?php echo base_url()."usr" ?>"  role="button" aria-expanded="false"><i class="fa big-icon fa-wheelchair"></i> <span class="mini-dn">Pengguna </span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- Header top area start-->
            <div class="content-inner-all">
                <div class="header-top-area">
                    <div class="fixed-header-top">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                                    <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                    <div class="admin-logo logo-wrap-pro">
                                        <!-- <a href="#"><img src="img/logo/log.png" alt="" /> -->
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">
                                    <div class="header-top-menu tabl-d-n">
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="nav-item">
                                                <a href="<?php echo base_url()."controls/logout" ?>"><span class="adminpro-icon adminpro-locked author-log-ic animated flipInX"></span>Log Out</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Header top area end-->
                <!-- Mobile Menu start -->
                <div class="mobile-menu-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul class="mobile-menu-nav">
                                            <li><a data-toggle="collapse" data-target="#Charts" href="#">Home <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                <ul class="collapse dropdown-header-top">
                                                    <li><a href="<?php echo base_url()."index.php/controls/dashboard" ?>">Dashboard </a>
                                                    </ul>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#demo" href="<?php echo base_url()."dodata" ?>">Daftar Pelanggan <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#others" href="<?php echo base_url()."dogol" ?>">Tarif Golongan <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="<?php echo base_url()."dobbn" ?>">Biaya Beban <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#Chartsmob" href="<?php echo base_url()."pen" ?>">Daftar Tagihan <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#Tablesmob" href="<?php echo base_url()."kettp" ?>">Daftar Pembayaran <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#formsmob" href="<?php echo base_url()."lap" ?>">Laporan <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                                <li><a data-toggle="collapse" data-target="#formsmob" href="<?php echo base_url()."usr" ?>">Pengguna <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>