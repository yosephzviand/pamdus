
<div class="breadcome-area mg-b-30 small-dn">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcome-list map-mg-t-40-gl animated zoomInDown shadow-reset">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-heading">
                                <div class="col-lg-1">
                                    <img src="<?php echo base_url('assets/tirta.png')?>">
                                </div>
                                <h1><b>Sistem Informasi PAM Dusun Kelor</b></h1>
                                <h1>
                                    <b>"TIRTA RAHAYU"</b>
                                </h1>
                                <h2>Dusun Kelor, Desa Kelor, Kec. Karangmojo, Kab. Gunungkidul</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="income-order-visit-user-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="income-dashone-total animated zoomInDown shadow-reset nt-mg-b-30">
                    <div class="income-title">
                        <div class="main-income-head">
                            <h2>Pelanggan</h2>
                            <div class="main-income-phara">
                                <p>Today</p>
                            </div>
                        </div>
                    </div>
                    <div class="income-dashone-pro">
                        <div class="income-rate-total">
                            <div class="price-adminpro-rate">
                                <h3><span class="counter"><?php echo number_format($pelanggan,0) ?></span></h3>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="income-dashone-total animated zoomInDown shadow-reset nt-mg-b-30">
                    <div class="income-title">
                        <div class="main-income-head">
                            <h2>Tagihan</h2>
                            <div class="main-income-phara order-cl">
                                <p>Today</p>
                            </div>
                        </div>
                    </div>
                    <div class="income-dashone-pro">
                        <div class="income-rate-total">
                            <div class="price-adminpro-rate">
                                <h3><span>Rp. </span><span class="counter"><?php echo number_format($tagihan,0) ?></span></h3>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="income-dashone-total animated zoomInDown shadow-reset nt-mg-b-30">
                    <div class="income-title">
                        <div class="main-income-head">
                            <h2>Pembayaran</h2>
                            <div class="main-income-phara visitor-cl">
                                <p>Today</p>
                            </div>
                        </div>
                    </div>
                    <div class="income-dashone-pro">
                        <div class="income-rate-total">
                            <div class="price-adminpro-rate">
                                <h3><span>Rp. </span><span class="counter"><?php echo number_format($bayar,0) ?></span></h3>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="income-dashone-total animated zoomInDown shadow-reset nt-mg-b-30">
                    <div class="income-title">
                        <div class="main-income-head">
                            <h2>Tunggakan</h2>
                            <div class="main-income-phara low-value-cl">
                                <p>Today</p>
                            </div>
                        </div>
                    </div>
                    <div class="income-dashone-pro">
                        <div class="income-rate-total">
                            <div class="price-adminpro-rate">
                                <h3><span>Rp. </span><span class="counter"><?php echo number_format($tunggakan,0) ?></span></h3>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>